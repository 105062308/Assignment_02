var enterKey;
var title, title2;

var menuState = {

    preload: function(){
        game.load.image('background', 'img/bg.jpg');
        enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        SKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
        //game.state.start('main');

        
         
        setTimeout(function(){
            console.log('did');
            
            title.setText("Press Enter to Enter the Game!!");
            
            title2.setText("Press S to See Score Board!!");
        },1000);
        
    },

    create: function(){
        game.add.image(0, 0, 'background');
        game.camera.flash(500, 1000);
    },
    
    update: function(){

        var Style = {font: "40px Papyrus", fill: "white" }

        if(enterKey.isDown)
            game.state.start('main');
        else if(SKey.isDown)
            game.state.start('end');


    
        title = game.add.text(10, 400, "", Style);
        title2 = game.add.text(10, 500, "", Style);
    }

}