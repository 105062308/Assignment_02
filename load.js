var loadState = {
    
    preload: function(){

        setTimeout(function(){
            title.setText("Loading ..... ");
        },1);

        game.load.spritesheet("player",'img/player.png', 32, 32);
        game.load.spritesheet('fake', 'img/fake.png', 96, 36);
        game.load.spritesheet('right', 'img/right.png', 96, 16);
        game.load.spritesheet('left', 'img/left.png', 96, 16);
        game.load.spritesheet('jump', 'img/jump.png', 96, 22);

        game.load.image("stairs", 'img/stairs.png');
        game.load.image("ceiling", 'img/ceiling.png');
        game.load.image("stab", 'img/stab.png');
        game.load.image("wall", 'img/wall.png');

        
        game.load.audio('bgmMusic', 'img/bg.mp3');
        game.load.audio('jump_voice','img/jump.wav');
        game.load.audio('fake_voice','img/fake.wav');
        game.load.audio('stair_voice','img/stairs.wav');
        game.load.audio('stab_voice','img/stab.wav');
        game.load.audio('game_over_voice', 'img/game_over.mp3');
        game.load.audio('move_voice', 'img/move.wav');
        game.load.audio('level_voice', 'img/level.mp3');
        var style = {font: "40px Papyrus", fill: "#ff0044" }
        title = game.add.text(200, 250, "", style);
    },

    create: function() {
        
    }
    ,update: function(){
        // Go to the menu state
        game.state.start('menu');  

    }
}