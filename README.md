# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu=> game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed



### My WebsiteL : https://105062308.gitlab.io/Assignment_02



### Menu

### 首頁：

剛進入遊戲時，可以按下`S`查看排行榜或是`Enter`開始遊戲    
如果選擇排行榜，就可以看到利用`firebase`建立的排行榜  
如果按下Enter，就會進入小朋友下樓梯的遊戲



***

### 遊戲：

按下Enter之後，就會進入遊戲的畫面  
遊戲中主要有五種不同的階梯

|  階梯樣式   | 功能 |
|:--------------:|:-----|
| 藍色階梯 | 這是一般的階梯，沒有特別的功能|
| 綠色彈簧 | 當小朋友跳到彈簧上之後，會有上升的速度並彈出|
| 米色磚塊 | 當小朋友踩上去之後，過一段時間便會翻轉，使小朋友下落|
| 傳送帶 | 小朋友在傳送帶上面時，會依據傳送帶不同的方向，而相對的向x移動|
| 刺刺 | 小朋友踩到刺刺的時候會被扣寫QQ |


***

### 分數以及血量：

分數以及以及血量分別用`player.floor`和`player.life`來記錄  
初始血量是10滴血，而樓層數則是從0開始   
每當多生成一個新的階梯，玩家的分數就會增加1   
而玩家的血量會有相對應的加減規則


**當玩家踩到刺，或是碰到最上面的尖刺，會被扣3滴血**  
**當玩家踩到階梯、米色磚塊、傳送帶時，會增加1滴血**  
**當關卡進入到後期時，踩到彈簧也會增加1滴血**


***

### 遊戲結束

當遊戲結束後，會跳出一個prompt的視窗   
此時玩家可以輸入自己的名字，如果有超過前三名，就會顯示於排行榜上   
此時玩家可以按下`M`或是`S`以回到初始畫面或開始遊戲

***

### 遊戲特別設計：

為了避免遊戲單一沒有挑戰，因此設計每一次遊戲都會從逐漸變更難度   
一開始方塊之間的間距較短，玩家也有較多的時間可以選擇要跳到哪一階   
而後每過約`20`階層，就會**增加難度**   
**增加難度**主要會增加階梯上升的速度，也會加大每一個階梯之間的間隔    
最大的間隔至少會讓一個畫面中有一個階梯    

而原先不會加血量的彈簧，也會在這時候開始作用(可以回寫)      
此外，為了避免連續遇到兩個翻轉方塊    
當關卡到達一定的難度後，就不會出現翻轉方塊


***
### State：
![State](img/state.png)

上圖是這次遊戲中的`State`流程圖

***

### 音效

利用`game.load.audio`，將各類的音樂檔 load 入   
遊戲開始後會有背景音樂，踩到不同的階梯也有不同的音效   
此外，當切換關卡時，也有加入音效

***


### 圖片

小朋友下樓梯的人物、翻轉方塊、彈簧、傳輸帶都是一個SpriteSheet   
利用`game.load.spritesheet`將圖片 load 進來   
`game.load.image`則是 load 固定不動的圖片   
像是天花板、兩旁的石磚邊界、刺刺、普通的階梯等等

***

### floor的增加

宣告一個空的陣列，利用亂數來隨機生成一個方塊   
生成後將floor加上以下的特性

``` 
game.physics.arcade.enable(this.floor);
this.floor.body.immovable = true;
floors.push(this.floor);

this.floor.body.checkCollision.down = false;
this.floor.body.checkCollision.left = true;
this.floor.body.checkCollision.right = true;
```

***

### player 的 碰撞偵測

對於階梯的碰撞，主要是利用`collide`的Funciton偵測   
並且呼叫`call_back function`   
以下為其中一段code
```javascript=101
callback_act: function(player, floor){
    if(floor.key == 'right') {
        player.body.x += 2;             //傳送帶將腳色向右傳動
        if (player.touch !== floor) {
            if(player.life < 10) {
                player.life += 1;       //腳色回血 
            }
            move_voice.play();          //播放音效
            move_voice.volume = 0.2;
            player.touch = floor;
        }
    }
```
而其他較簡單的偵測，則沒有呼叫call_back function   
直接使用簡單的collide偵測碰撞

***

### 物理引擎以及動畫

小朋友的下墜使用gravity，使其可以有重力加速度下降


`game.physics.arcade.enable(this.player);`    
`this.player.body.gravity.y = 800;`

而小朋友要顯示的動畫則是利用當前的速度來偵測   
有向左、向右走，還有向下掉，利用`velocity`的正負以及是否為 0 來控制

***

### firebase Score Board

每一次的樓層數，都會上傳到`firebase realtime database`裡面   
當遊戲結束時，利用 firebase 的 `OrderByChild function` 遍歷紀錄   
最後利用`unshift`將記錄由高到低放置到陣列裡面，並且顯示出來


***
