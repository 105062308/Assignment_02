var game = new Phaser.Game( 585, 600, Phaser.auto, "canvas");

game.state.add('boot',bootState);
game.state.add('load',loadState);
game.state.add('menu',menuState);
game.state.add('main', mainState);
game.state.add('end',endState);

game.state.start('boot');