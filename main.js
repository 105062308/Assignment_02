
var time_interval = 800;
var floors = [];
var Last = 0, stair_level = 1;
var up_velocity = 2;
var jump_voice, fake_voice, stair_voice, stab_voice, game_over_voice, move_voice, level_voice;

var mainState = {

    create: function(){

        //initial
        game.stage.backgroundColor = '#00000'; // Set background
        game.renderer.renderSession.roundPixels = true; // Setup renderer
        game.renderer.renderSession.roundPixels = true;
        stair_level = 1;
        Last = 0;
        time_interval = 800;
        up_velocity = 2;

        

        //keyboard

        this.cursor = game.input.keyboard.createCursorKeys();


        //set player 
        this.player = game.add.sprite( game.width/2, 100,'player'); 
		this.player.scale.setTo(1.5, 1.5); 
        game.physics.arcade.enable(this.player);    
        this.player.body.gravity.y = 800;

        //player animation

        this.player.animations.add('left_walk', [0 ,1, 2, 3], 8, false);
        this.player.animations.add('right_walk', [9 ,10, 11, 12], 12, false);
        this.player.animations.add('left_jump', [18 ,19, 20, 21], 8, false);
        this.player.animations.add('right_jump', [27 ,28, 29, 30], 8, false);
        this.player.animations.add('jump', [36 ,37, 38, 39], 8, false);
        this.player.life = 10;
        this.player.touch = "";
        this.player.floor = 0;

        //wall and ceiling
        this.left_wall = game.add.sprite(0, 0, 'wall');
        this.right_wall = game.add.sprite(570, 0, 'wall');
        this.ceiling = game.add.sprite(0,0,'ceiling');
        this.ceiling.scale.setTo(1.5,1);
        game.physics.arcade.enable(this.left_wall);
        game.physics.arcade.enable(this.right_wall);
        game.physics.arcade.enable(this.ceiling, this.TopCollide);
        this.right_wall.body.immovable = true;
        this.left_wall.body.immovable = true;  
        this.ceiling.body.immovable = true;


        
        //audio

        jump_voice = game.add.audio('jump_voice');
        fake_voice = game.add.audio('fake_voice');
        stair_voice = game.add.audio('stair_voice');
        stab_voice = game.add.audio('stab_voice');
        move_voice = game.add.audio('move_voice');
        level_voice = game.add.audio('level_voice');
        game_over_voice = game.add.audio('game_over_voice');
        
       
        this.bgmMusic = game.add.audio('bgmMusic');
        this.bgmMusic.loop = true;
        this.bgmMusic.play();
        this.bgmMusic.volume = 0.5;


        //text

        var style = {fill: '#ffffff', fontSize: '40px',  fontFamily : 'sans-serif' }
        text1 = game.add.text(50, 10, '', style);
        text2 = game.add.text(400, 10, '', style);
        text3 = game.add.text(140, 200, 'Enter 重新開始', style);
        text3.visible = false;


    },

    update: function(){

        if (!this.player.inWorld || this.player.life <= 0){
            this.bgmMusic.pause();
            this.playerDie();
        }
            
        this.moveplayer();
        this.flappyFloor();

        game.physics.arcade.collide(this.left_wall, this.player);
        game.physics.arcade.collide(this.right_wall, this.player);
        game.physics.arcade.collide(this.player, floors, this.callback_act);
        game.physics.arcade.collide(this.player, this.ceiling, this.TopCollide);

        this.floorUp();

        text1.setText('life:' + this.player.life);
        text2.setText('第' + this.player.floor + '層');
    },

    playerDie: function() { 
        
        const fb = firebase.database().ref();

        floors.forEach(function(s) {s.destroy()});
        floors = [];
        entry = null;
        entry=prompt("Your Name!!! : ","");

        if(entry){
            data = {
                Name: entry,
                floor: this.player.floor
            };
    
            fb.child('/record').push(data);
        }
        console.log(typeof(entry));
        game_over_voice.play();
        game_over_voice.volume = 0.3;

        

        game.state.start('end');
    },

    flappyFloor: function(){
        if(game.time.now > ((Last==0)? 0 : Last + time_interval)){
            Last = game.time.now;
            this.addFloor();
            this.player.floor ++;
            
            if(this.player.floor > stair_level +20 && time_interval < 3500){
                console.log(stair_level, Last);
                stair_level = this.player.floor;
                time_interval += 200;
                up_velocity += 0.3;
                setTimeout(function() {
                    level_voice.play();
                }, 200);
            }
        }
        
    },

    addFloor: function(){

        
        var rand = Math.random()*100;
        var x = Math.random()*450 + 20;
        var y = 600;

        if(rand>=20 && rand<40){
            this.floor = game.add.sprite(x, y, 'stab');
        }
        else if(rand>=40 && rand<60){
            this.floor = game.add.sprite(x, y, 'jump');
            this.floor.animations.add('jump', [0, 1, 2, 3], 16, true);
            this.floor.frame = 0;
        }
        else if(rand>=60 && rand<70){
            this.floor = game.add.sprite(x, y, 'right');
            this.floor.animations.add('act', [0, 1, 2, 3], 16, true);
            this.floor.play('act');
        }
        else if(rand >=70 && rand<80){
            this.floor = game.add.sprite(x, y, 'left');
            this.floor.animations.add('act', [0, 1, 2, 3], 16, true);
            this.floor.play('act');
        }
        else if(rand>=80 && time_interval<2000){
            this.floor = game.add.sprite(x, y, 'fake');
            this.floor.animations.add('fake', [0, 1, 2, 3], 16, true);
            this.floor.frame = 0;
        }else{
            this.floor = game.add.sprite(x, y, 'stairs');
        }

        game.physics.arcade.enable(this.floor);
        this.floor.body.immovable = true;
        floors.push(this.floor);

        this.floor.body.checkCollision.down = false;
        this.floor.body.checkCollision.left = true;
        this.floor.body.checkCollision.right = true;

    },

    floorUp: function(){
        for(var i=0; i<floors.length; i++) {
            var floor = floors[i];
            floor.body.position.y -= up_velocity;
            if(floor.body.position.y <= -20) {
                floor.destroy();
                floors.splice(i, 1);
            }
        }
    },

    moveplayer: function(){

        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -400;
        }
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x = 400;
        }else{
            this.player.body.velocity.x = 0;
        }
        this.SetPlayerAnimation();
        
    },

    TopCollide: function(player, ceiling){
        player.body.y += 20;
        game.camera.flash(5000, 1000);
        player.life -=3;
        stab_voice.play();
    },

    callback_act: function(player, floor){
        if(floor.key == 'right') {
            player.body.x += 2;
            if (player.touch !== floor) {
                if(player.life < 10) {
                    player.life += 1;
                }
                move_voice.play();
                move_voice.volume = 0.2;
                player.touch = floor;
            }
        }
        if(floor.key == 'left') {
            player.body.x -= 2;
            if (player.touch !== floor) {
                if(player.life < 10) {
                    player.life += 1;
                }
                move_voice.play();
                move_voice.volume = 0.2;
                player.touch = floor;
            }
        }
        if(floor.key == 'jump') {
            if(time_interval>1500 && player.life<10){
                player.life++;
            }
            jump_voice.play();
            jump_voice.volume = 0.2;
            player.body.velocity.y = -400;
            floor.animations.play('jump');
            setTimeout(function() {
                floor.animations.stop(null,true);
            }, 200);
            floor.body.checkCollision.left = false;
            floor.body.checkCollision.right = false;
        }
        if(floor.key == 'stairs') {
            if (player.touch !== floor) {
                if(player.life < 10) {
                    player.life += 1;
                }
                stair_voice.play();
                player.touch = floor;
            }
        }
        if(floor.key == 'stab') {
            if (player.touch !== floor) {
                stab_voice.play();
                player.life -= 3;
                player.touch = floor;
                game.camera.flash(5000, 1000);
                floor.body.checkCollision.left = false;
                floor.body.checkCollision.right = false;
            }
               
            floor.body.setSize(96, 15, 0, 20);
        }
        if(floor.key == 'fake') {
            if(player.touch !== floor) {
                if(player.life < 10) {
                    player.life += 1;
                }
                floor.animations.play('fake');
                setTimeout(function() {
                            
                    fake_voice.play();
                    floor.body.checkCollision.up = false;
                    floor.animations.stop(null,true);
                }, 150);
                player.touch = floor;
            }
            
        }
    },


    SetPlayerAnimation: function(){
        var x = this.player.body.velocity.x;
        var y = this.player.body.velocity.y;
        
        if( x>0 && y==0){
            this.player.animations.play('right_walk');
        }
        else if( x<0 && y==0 ){
            this.player.animations.play('left_walk');
        }
        else if( x>0 && y>0 ){
            this.player.animations.play('right_jump');
        }
        else if( x<0 && y>0 ){
            this.player.animations.play('left_jump');
        }
        else if( x==0 && y>0 && y.toFixed(5)!=13.33333){
            this.player.animations.play('jump');
        }
        else{
            this.player.frame = 8;
            this.player.animations.stop();
        }

    }
};
