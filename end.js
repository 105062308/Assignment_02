var enterKey;
var clickCount = 0;


var three = [];
// We'll also need to keep track of the text object that we add to the game.
var clickCounter;

var endState = {

    preload: function(){

        game.load.bitmapFont('desyrel', 'assets/fonts/bitmapFonts/desyrel.png', 'assets/fonts/bitmapFonts/desyrel.xml');

        game.load.image('background', 'img/bg.png');
        MKey = game.input.keyboard.addKey(Phaser.Keyboard.M);
        SKey = game.input.keyboard.addKey(Phaser.Keyboard.S);

        
        setTimeout(function(){
            console.log('did');
            title.setText("Name          Score");
            text1.setText(three[0][0]);
            text2.setText(three[1][0]);
            text3.setText(three[2][0]);
            text4.setText(three[0][1]);
            text5.setText(three[1][1]);
            text6.setText(three[2][1]);
            
            back.setText("M : Menu");
            start.setText("S : Start");
        },1000);

    },

    create: function(){
        //game.add.image(0, 0, 'background');
        game.camera.flash(5000, 1000);
        
        const fb = firebase.database().ref('record');
        fb.orderByChild('floor').once('value').then(function(snapshot){
            snapshot.forEach(function (childSnapshot){
                three.unshift([childSnapshot.val().Name,childSnapshot.val().floor]);
            })
        })
    },
    
    update: function(){
        
        if(SKey.isDown)
            game.state.start('main');
        if(MKey.isDown)
            game.state.start('menu');
        var style = {font: "40px Papyrus", fill: "#ff0044" }
        var GoldStyle = {font: "40px Papyrus", fill: "gold" }
        var SilverStyle = {font: "40px Papyrus", fill: "silver" }
        var BonzeStyle = {font: "40px Papyrus", fill: "#CD7F32" }


        title = game.add.text(150, 100, "", style);
        text1 = game.add.text(150, 200, '', GoldStyle);
        text2 = game.add.text(150, 300, '', SilverStyle);
        text3 = game.add.text(150, 400, '', BonzeStyle);

        text4 = game.add.text(350, 200, '', GoldStyle);
        text5 = game.add.text(350, 300, '', SilverStyle);
        text6 = game.add.text(350, 400, '', BonzeStyle);
        
        back = game.add.text(50, 500, " ", style);
        start = game.add.text(350, 500, " ", style);
    }
    
}